# Valkyrie

WordPress child theme for [Septera](https://wordpress.org/themes/septera/) and [Bravada](https://wordpress.org/themes/bravada/) by Cryout Creations.

![valkyrie](screenshot.png)

Created for [Chicago Gang History II](https://vhs.codeberg.page/site/chicago-gang-history-ii) in 2017 by [VHS](https://vhs.codeberg.page). Updated in 2021 for [the third installment](https://vhs.codeberg.page/site/chicago-gang-history-iii).

## Usage

Remove the `septera` or  `bravada` file extensions depending your WP theme and copy the files from this repo to a `valkyrie` folder under `wp-content/themes` or zip up the files and install from WordPress Admin.
## Features

- Use Page Excerpts for Icon Block content
- Enable Measurement Protocol (GA4)
- Enable AdSense Auto ads
- Vanity footer links
- Add custom CSS styles
- Subscription form widget styles
- Bravada: Add Custom HTML (disabled by default)
- Septera: External background image

## Rights

Valkyrie - WordPress child theme for Septera and Bravada by Cryout Creations.<br>
Copyright (C)&nbsp;&nbsp;2017-2019, 2021 &lt;vhsdev@tutanota.com&gt; (https://vhs.codeberg.page)

Valkyrie is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Valkyrie art [by Brian](https://my.desktopnexus.com/Talislanta/) and copyright its respective owner(s).
